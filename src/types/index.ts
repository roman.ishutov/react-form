export type TState = { [key: string]: unknown };

export type TAction = {
  type: string;
  types?: Array<string>,
  request?: () => Promise<unknown>,
  data?: unknown
}

export type TConfig = {
  method?: string,
  url?: string,
  headers?: { [key: string]: string },
  data?: FormData,
  responseType: string
}

export type TResponseHandlerType = { [key: string]: (data: Response) => unknown }

export type TPropsList = {
  loadListOfFiles: () => void,
  deleteFile: (path: string) => void,
  list: Array<string>
}

export type TResponseList = {
  name: string,
  extension: string,
  fileSizeInBytes: number
}

export type TPropsUpload = {
  uploadFileAction: (file: FormData) => void
}

export type TInput = {
  sampleFile: {
    files: Array<File>
  }
}
