export const BASE_URL = 'http://localhost:8000';
export const URL_NOT_FOUND = 'Incorrect Url';
export const ROOT_NOT_FOUND = 'Root not found';

export const SET_SELECTED_ACTION_TYPE = 'setSelectedItem';

export const RESPONSE_TYPES = {
  JSON: 'json',
  TEXT: 'text',
  BLOB: 'blob'
};

export const METHODS_HTTP = {
  GET_METHOD: 'GET',
  POST_METHOD: 'POST',
  DELETE_METHOD: 'DELETE'
};

export const ENDPOINTS = {
  LIST_URL: '/list',
  UPLOAD_URL: '/upload',
  DOWNLOAD_URL: '/files/',
  DELETE_URL: '/list/'

};

export const LIST_GET_ACTION_TYPES = {
  GET_LIST_PENDING: 'getListPending',
  GET_LIST_SUCCESS: 'getListSuccess',
  GET_LIST_ERROR: 'getListError'
};

export const REMOVE_FILE_ACTION_TYPES = {
  DELETE_FILE_PENDING: 'deleteFilePending',
  DELETE_FILE_SUCCESS: 'deleteFileSuccess',
  DELETE_FILE_ERROR: 'deleteFileError'
};

export const DOWNLOAD_FILE_ACTION_TYPES = {
  DOWNLOAD_FILE_PENDING: 'downloadFilePending',
  DOWNLOAD_FILE_SUCCESS: 'downloadFileSuccess',
  DOWNLOAD_FILE_ERROR: 'downloadFileError'
};

export const UPLOAD_FILE_ACTION_TYPES = {
  UPLOAD_FILE_PENDING: 'uploadFilePending',
  UPLOAD_FILE_SUCCESS: 'uploadFileSuccess',
  UPLOAD_FILE_ERROR: 'uploadFileError'
};


