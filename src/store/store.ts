import { Store, createConnect, apiMiddleware, thunk } from 'react-lib';
import fileListReducer from './reducers';

const store = new Store({ fileListReducer }, [apiMiddleware, thunk]);

const connect = createConnect(store);

export { connect };
