import http from './HttpRequest';
import { ENDPOINTS, RESPONSE_TYPES } from '@const';

const { LIST_URL,
  DELETE_URL, UPLOAD_URL, DOWNLOAD_URL } = ENDPOINTS;
const { JSON,
  TEXT, BLOB } = RESPONSE_TYPES;


function loadFileList() {
  return http.get(LIST_URL, { responseType: JSON });
}

function uploadFile(data: FormData) {
  return http.post(UPLOAD_URL, data, { responseType: TEXT });
}

function downloadFile(path: string) {
  return http.get(`${DOWNLOAD_URL}${path}`, { responseType: BLOB });
}

function removeFile(path: string) {
  return http.delete(`${DELETE_URL}${path}`, { responseType: TEXT }).then(() => path);
}

export {
  loadFileList,
  uploadFile,
  downloadFile,
  removeFile
};
