import { TResponseHandlerType, TConfig } from '@types';
import { BASE_URL, URL_NOT_FOUND, METHODS_HTTP, RESPONSE_TYPES } from '@const';

const { JSON, TEXT, BLOB } = RESPONSE_TYPES;


const responseTypeHandlers: TResponseHandlerType = {
  [JSON]: (response: { json: () => any; }) => response.json(),
  [TEXT]: (response: { text: () => any; }) => response.text(),
  [BLOB]: (response: { blob: () => any; }) => response.blob()
};

class HttpRequest {
  constructor(public baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  get(url: string, config: TConfig) {
    return this.request(Object.assign({}, config, { method: METHODS_HTTP.GET_METHOD, url }));
  }

  post(url: string, data: FormData, config: TConfig) {
    return this.request(Object.assign({}, config, { method: METHODS_HTTP.POST_METHOD, url, data }));
  }

  delete(url: string, config: TConfig) {
    return this.request(Object.assign({}, config, { method: METHODS_HTTP.DELETE_METHOD, url }));
  }

  request(config: TConfig) {
    const {
      method = METHODS_HTTP.GET_METHOD,
      url,
      headers,
      data,
      responseType = TEXT
    } = config;

    if (!url) {
      throw new Error(URL_NOT_FOUND);
    }

    const baseUrl = new URL(url, this.baseUrl);

    return fetch(baseUrl.href, {
      method,
      headers,
      body: data
    })
      .then(res => {
        if (!res.ok) {
          throw new Error(`${method} ${res.url} ${res.statusText}`);
        }

        return responseTypeHandlers[responseType](res);
      });
  }
}

export default new HttpRequest(BASE_URL);
