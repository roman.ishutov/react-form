import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

const mode = process.env.NODE_ENV;
const isProd = mode === 'production';
const config = {
  mode,
  resolve: {
    alias: {
      '@types': path.resolve(__dirname, './src/types'),
      '@const': path.resolve(__dirname, './src/const'),
      '@api': path.resolve(__dirname, './src/api/index'),
      '@store': path.resolve(__dirname, './src/store')
    }
  },
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd,
    lib: {
      entry: path.resolve(__dirname, 'lib/react-like/index.ts'),
      name: '[name]',
      fileName: ext => `[name]/[name].${ext}.ts`,
      formats: ['es', 'cjs']
    },
    rollupOptions: {
      input: {
        react: path.resolve(__dirname, 'src')
      },
      output: {
        assetFileNames: assetInfo => {
          let [, ext] = assetInfo.name.split('.');

          if ((/png|jpe?g|svg|gif/i).test(ext)) {
            ext = 'img';
          }
          return `${ext}/[name][hash][extname]`;
        }
      }
    },
    plugins: [
      react({
        babel: {
          plugins: [
            ['@babel/plugin-proposal-decorators', { 'legacy': true }],
            ['@babel/plugin-proposal-class-properties']
          ]
        }
      })
    ]
  }
};

export default defineConfig(config);